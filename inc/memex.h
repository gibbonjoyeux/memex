/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#ifndef MEMEX_H
#define MEMEX_H

////////////////////////////////////////////////////////////////////////////////
/// INCLUDES
////////////////////////////////////////////////////////////////////////////////

#include <stdlib.h>
#include <stdio.h>
#include <dirent.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "basics.h"
#include "string.h"

////////////////////////////////////////////////////////////////////////////////
/// DEFINES
////////////////////////////////////////////////////////////////////////////////

#define COPY_BUFFER			65536	// 2^16	Hard file copy buffer
#define PATH_BUFFER			4096	// 2^12 Path buffer
#define EXTRA_BUFFER		512		// 2^9	Tag extra buffer
#define VAR_BUFFER			256		// 2^8	Variable | parameter buffer
#define STACK_BUFFER		32		// 2^5	Tag stack buffer
#define STACK_NAME_BUFFER	32		// 2^5	Tag name stack buffer

#define NODE_DIR			0
#define NODE_TXT			1
#define NODE_HTML			2
#define NODE_OTHER			3

#define IS_TITLE(file, len, i)		(file[i] == '#')
#define IS_LIST(file, len, i)		(file[i] == '-' \
									&& i + 1 < len \
									&& IS_SPACE(file[i + 1]) == true)
#define IS_LINE(file, len, i)		(file[i] == '-' \
									&& i + 1 < len \
									&& file[i + 1] == '-')
#define IS_TABLE(file, len, i)		(file[i] == '|')
#define IS_HTML(file, len, i)		(file[i] == '<')
#define IS_TAG(file, len, i)		(file[i] == '-' \
									&& i + 1 < len \
									&& file[i + 1] == '>')
#define IS_CHECKLIST(file, len, i)	(file[i] == '[' \
									&& i + 2 < len \
									&& (file[i + 1] == 'x' \
									|| file[i + 1] == ' ') \
									&& file[i + 2] == ']')
#define IS_IMAGE(file, len, i)		(file[i] == '!' \
									&& i + 1 < len \
									&& file[i + 1] == '[')
#define IS_EXTRA(file, len, i)		(file[i] == '=' \
									&& i + 1 < len \
									&& file[i + 1] == '>')

////////////////////////////////////////////////////////////////////////////////
/// TYPEDEFS
////////////////////////////////////////////////////////////////////////////////

typedef DIR				t_dir;
typedef struct dirent	t_dir_entry;
typedef struct stat		t_stat;

typedef struct s_node	t_node;
typedef struct s_memex	t_memex;

////////////////////////////////////////////////////////////////////////////////
/// STRUCTURES
////////////////////////////////////////////////////////////////////////////////

struct			s_node {
	u8			type;			// Node type: DIRECTORY | HTML | OTHER
	t_node		*parent;		// Node parent
	char		*path;			// Node path

	bool		is_index;		// File: is index
	bool		is_template;	// File: is template
	size_t		length;			// Parent: num childs
	t_node		*index;			// Parent: index child
	t_node		**childs;		// Parent: childs array
};

struct			s_memex {
	char		url[PATH_BUFFER];		// Site base url
	char		content[COPY_BUFFER];
	char		path[PATH_BUFFER];
	char		extra[EXTRA_BUFFER];
	int			index;
	t_node		*tree;		// Site tree
	t_node		*node;		// Active tree node
	struct {
		char	stack[STACK_BUFFER][STACK_NAME_BUFFER];
		int		index;
	}			tag;
};

extern t_memex	g;

////////////////////////////////////////////////////////////////////////////////
/// FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

t_node			*build_tree(void);
bool			compute_tree(t_node *node);
bool			compute_sitemap(t_node *node);
bool			compute_file(t_node *node);
bool			compute_file_txt(t_node *node, int fd_out);
bool			compute_file_html(char *path, int fd_out);
bool			compute_file_other(char *path, int fd_out);
void			compute_variable_title(int fd_out);
void			compute_variable_template(char *name, int fd_out);
void			compute_variable_content(int fd_out);
void			compute_variable_siblings(char *param, int fd_out);
void			compute_variable_childs(int fd_out);
void			compute_variable_sitemap(int fd_out);
void			compute_variable_tree(char *param, int fd_out);
void			compute_variable_link(char *param, int fd_out);
void			compute_variable_tag(char *param, int fd_out);
void			compute_variable_url(int fd_out);

bool			compute_node_link(t_node *node, bool active, char *buffer);
void			compute_node_title(t_node *node, char *buffer);
void			compute_node_path(t_node *node, char *buffer);

void			print_tag(char *tag, bool mode, int fd_out);
void			print_string(char *file, int len, int fd_out);
void			print_block_title(char *file, int len, int *i, int fd_out);
void			print_block_line(char *file, int len, int *i, int fd_out);
void			print_block_list(char *file, int len, int *i, int fd_out);
void			print_block_checklist(char *file, int len, int *i, int fd_out);
void			print_block_table(char *file, int len, int *i, int fd_out);
void			print_block_paragraph(char *file, int len, int *i, int fd_out);
void			print_block_image(char *file, int len, int *i, int fd_out);
void			print_block_tag(char *file, int len, int *i, int fd_out);
void			print_block_html(char *file, int len, int *i, int fd_out);

void			skip_html(char *file, int len, int *i);
bool			is_html(char *path);
bool			is_txt(char *path);

#endif

