# memex

Small and minimal static website generator.
C based program mainly thought for Memex / Wiki websites.

## HOW

### BUILD

Simply run the `build.sh` script :)

It basically clones memex libraries (2 small libraries), build these libraries
and build the `memex` binary (executable).

### RUN

Memex only requires the site directory. An extra parameter can be provided
to specify the website url
(usefully paired with the ${URL}, ${TREE}, etc. variables).

The website url can be a local path like `file:///Users/.../site/` to
allow your browser to read your website without server.

```
> ./memex website-directory/
```

```
> ./memex website-directory/ web/site/url/
```

## ORGANISATION

Memex builds your website into a `build/` directory directly into your
site directory.

It handles two kinds of files, *.html* and *.txt* files.

*.html* files are translated using the markdown notation and variables.

*.txt* work the same way but will look for a `template.html` file to
format their html content. That way, they can contain only the main
content. `template.html` is searched into the .txt directory, if it not
found, it will be searched into the parent directory and so on
(I personally only use one `template.html` file which is in the
main directory). `template.html` should contain the `${CONTENT}` variable
which will insert the `.txt` file.

Other files are simply copied without any change.

## VARIABLES

Variables are noted like this `${VARIABLE}`, "VARIABLE" being the variable
name. Optionnal parameters can be passed to variables like so
`${VARIABLE:param1:param2}`.

Variables are replaced in text by special content. You can use it to get
your sitemap, a page's childrens pages, a page's hierarchy, or to insert
a whole file.

- ${URL}			insert site url
- ${CONTENT}		insert .txt content (template)
- ${TITLE}			insert page title
- ${SIBLINGS}		insert page siblings
- ${SIBLINGS:t}		insert page siblings (<t>link</t>)
- ${CHILDS}			insert page childs as nested lists
- ${CHILDS:t1}		NOT YET WORKING - insert page childs
- ${CHILDS:t1:t2}	NOT YET WORKING - insert page childs
- ${TREE}			insert page tree (siblings & ancestors tree)
- ${TREE:t1}		insert page tree (<t1>link</t1>)
- ${TREE:t1:t2}		insert page tree (<t2><t1>link1</t1><t1>link2</t1></t2>)
- ${SITEMAP}		insert site tree as nested lists (all site pages)
- ${SITEMAP:t1}		NOT YET WORKING - insert site tree
- ${SITEMAP:t1:t2}	NOT YET WORKING - insert site tree
- ${LINK:url}		insert link
- ${LINK:url:text}	insert link
- ${TAG:tag}		insert tag without affecting markdown
- ${path.template}	insert 'path.template' file here

## NOTATION

The memex notation is highly inspired by the markdown notation.
I kept what I considered useful and added some extras.

Notation example

```
	-> ARTICLE
		-> NAV
			- [link 1](url/1)
			- [link 2](url/2)
			- [link 3](url/3)
		-> NAV

		-> SECTION
			# title

			Description text...

			## subtitle

			Article content here...

			![my image](image/url)

			Article content here...
			With an [extra link](link/url) here...

		-> SECTION

		-> SECTION

			# another title

			=> class=special-table

			| cell 1 | cell 2 | cell 3 |
			| cell 4 | cell 5 | cell 6 |
			| cell 7 | cell 8 | cell 9 |

		-> SECTION
	-> ARTICLE
```

### PARAGRAPHS

Close lines are grouped into `<p>...</p>` tags.

```
	this is a line
	this is another line

	this line is grouped into another paragraph
```

```
	<p>
		this is a line
		this is another line
	</p>

	<p>
		this line is grouped into another paragraph
	</p>
```

### TITLES

```
	# this is my title

	## this is my subtitle
```

```
	<h1>this is my title</h1>
	<h2>this is my subtitle</h2>
```

### LISTS

```
	- item 1
	- item 2
	- item 3
		- item 3.a
		- item 3.b
		- item 3.c
	- item 4
```

```
	<ul>
		<li>item 1</li>
		<li>item 2</li>
		<li>item 3
			<ul>
				<li>item 3.a</li>
				<li>item 3.b</li>
				<li>item 3.c</li>
			</ul>
		</li>
		<li>item 4</li>
	</ul>
```

### CHECK LISTS

```
	[x] checked item
	[ ] unchecked item
	[x] checked item
		[x] checked subitem
		[x] checked subitem
```

```
	<ul class=checklist>
		<li class=on>check item</li>
		<li>unchecked item</li>
		<li class=on>checked item
			<ul>
				<li class=on>checked subitem</li>
				<li class=on>checked subitem</li>
			</ul>
		</li>
	</ul>
```

### TABLES

```
	|# title 1	| title 2	| title 3	|
	| item 4	| item 5	| item 6	|
	| item 7	| item 8	| item 9	|
```

```
	<table>
		<tr>
			<th>title 1</th>
			<th>title 2</th>
			<th>title 3</th>
		</tr>
		<tr>
			<td>item 4</td>
			<td>item 5</td>
			<td>item 6</td>
		</tr>
		<tr>
			<td>item 7</td>
			<td>item 8</td>
			<td>item 9</td>
		</tr>
	</table>
```

### HORIZONTAL LINES

``` --- ```

``` <hr> ```

### IMAGES

``` ![alt text](image/url) ```

``` <img url="image/url" alt="alt text"> ```

### LINKS

``` This is the [link](site/url) to my website ```

``` This is the <a href="site/url">link</a> to my website ```

### EMPHASIS

``` This is an *important* word ```

``` This is an <em>important</em> word ```

### HTML TAG

HTML Tags are skiped, special variables (${...}) still work
but not the memex notation

```
	<div>
		this is not formatted
	</div>

	While this is *formatted*
```

```
	<div>
		this is not *formatted*
	</div>

	<p>While this is <em>formatted</em></p>
```

### ACTIVE TAGS

Active tags are a readable way to keep html hierarchy without affecting
notation.
You open a tag by giving its name and close it by passing it again.
Active tags can be nested if they do no have the same tag name (case sensitive).

```
	-> ARTICLE
		-> SECTION
			
			This is the *first* section of my article

		-> SECTION

		-> SECTION

			This is the *second* section of my article

		-> SECTION
	-> ARTICLE
```

```
	<article>
		<section>
			<p>This is the <em>first</em> section of my article
		</section>
		<section>
			<p>This is the <em>second</em> section of my article
		</section>
	</article>
```

### TAG EXTRA

Tag extras is a notation that allows you to define `class` or `id` values
to notation blocks. It works with *active tags*, *paragraphs*, *lists*,
*checkedlists*, *tables*, *titles*, *images* and *horizontal lines*.

```
	=> class=my-special-list

	- item 1
	- item 2
	- item 3
```

```
	<ul class=my-special-list>
		<li>item 1</li>
		<li>item 2</li>
		<li>item 3</li>
	</ul>
```
