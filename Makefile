################################################################################
## SOURCES #####################################################################
################################################################################

SRCS		= 	$(wildcard src/*.c) \
				$(wildcard src/*/*.c)
OBJS		= 	$(SRCS:src/%.c=obj/%.o)
INCS		=	$(wildcard inc/*.h)

#$(info    	SOURCES -> $(SRCS))
#$(info    	OBJECTS -> $(OBJS))

NAME		= memex
CC			= gcc
INCLUDES	= -I inc/ \
				-I lib/c-basics/inc/ \
				-I lib/c-string/inc/
LIB			= lib/c-basics/c-basics.a \
				lib/c-string/c-string.a
FLAGS		= -Wall -Wextra -Werror -g -Wno-unused-private-field

ifdef DEBUG
FLAGS		=
endif

################################################################################
## RULES #######################################################################
################################################################################

##############################
## RULE BINARY ###############
##############################
all: $(NAME)

run: all
	@./$(NAME)

##############################
### RULE LIBRARY #############
##############################
$(NAME): $(OBJS)
	@echo "[lib/c-basics]" \
	&& [ -f lib/c-basics/c-basics.a ] \
	&& make -s -C lib/c-basics
	@echo "[lib/c-string]" \
	&& [ -f lib/c-string/c-string.a ] \
	&& make -s -C lib/c-string
	@$(CC) $(FLAGS) $(OBJS) $(LIB) -o $(NAME)

##############################
### RULE SOURCES #############
##############################
obj/%.o: src/%.c Makefile $(INCS)
	@mkdir -p $(dir $(@))
	@echo $(<:src/%=%)
	@$(CC) $(FLAGS) $(INCLUDES) -o $(@) -c $<

##############################
### RULE CLEANING ############
##############################
clean:
	@rm -rf $(OBJS)

fclean: clean
	@rm -rf $(NAME)

##############################
### RULE RESTART #############
##############################
re: fclean
	@make
