/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "memex.h"

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

static inline void
add_path(char *buffer, char *path) {
	int				i, j;

	/// FIND BUFFER END
	i = 0;
	while (buffer[i] != 0)
		i += 1;
	/// COPY MAIN PATH
	j = 0;
	while (path[j] != 0) {
		buffer[i] = path[j];
		i += 1;
		j += 1;
	}
	buffer[i] = 0;
	/// REPLACE .txt BY .html
	if (i >= 4 && str_equ(buffer + i - 4, ".txt") == true)
		str_cpy(buffer + i - 4, ".html");
}

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

bool			compute_node_link(t_node *node, bool active, char *buffer) {
	if (node->type == NODE_DIR && node->index == NULL)
		return false;
	/// PUT LINK OPENING
	if (active == true)
		str_cpy(buffer, "<a class=active href=\"");
	else
		str_cpy(buffer, "<a href=\"");
	/// PUT LINK URL
	str_cat(buffer, g.url);
	str_cat(buffer, "/");
	if (node->type == NODE_DIR)
		add_path(buffer, node->index->path);
	else
		add_path(buffer, node->path);
	str_cat(buffer, "\">");
	/// PUT LINK TITLE
	compute_node_title(node, buffer + str_len(buffer));
	/// PUT LINK CLOSING
	str_cat(buffer, "</a>");
	return true;
}
