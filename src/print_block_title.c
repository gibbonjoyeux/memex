/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "memex.h"

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

void
print_block_title(char *file, int len, int *i, int fd_out) {
	char			tag[3] = "h1";
	int				level;
	int				j, k;

	/// GET LEVEL
	j = *i;
	while (j < len && file[j] == '#')
		j += 1;
	level = j - *i;
	/// ERROR
	if (level > 6) {
		print_block_paragraph(file, len, i, fd_out);
		return;
	}
	/// GET TITLE
	k = j;
	while (k < len) {
		if (file[k] == '<')
			skip_html(file, len, &k);
		if (file[k] == '\n')
			break;
		k += 1;
	}
	/// PRINT TITLE
	tag[1] = '0' + level;
	print_tag(tag, true, fd_out);
	print_string(file + j, k - j, fd_out);
	print_tag(tag, false, fd_out);
	/// OFFSET FILE
	*i = k;
	if (k < len)
		*i += 1;
}
