/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "memex.h"

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

void		print_tag(char *tag, bool mode, int fd_out) {
	char	string[EXTRA_BUFFER + 32];
	int		i, j;

	if (tag[0] == 0)
		return;
	string[0] = '<';
	/// OPEN
	if (mode == true) {
		i = 1;
	/// CLOSE
	} else {
		string[1] = '/';
		i = 2;
	}
	/// TAG
	j = 0;
	while (tag[j] != 0) {
		string[i] = tag[j];
		i += 1;
		j += 1;
	}
	/// TAG EXTRA
	if (g.extra[0] != 0) {
		string[i] = ' ';
		i += 1;
		j = 0;
		while (g.extra[j] != 0) {
			string[i] = g.extra[j];
			i += 1;
			j += 1;
		}
		g.extra[0] = 0;
	}
	/// END
	string[i] = '>';
	write(fd_out, string, i + 1);
}
