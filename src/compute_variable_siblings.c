/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "memex.h"

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

void			compute_variable_siblings(char *param, int fd_out) {
	char		string[PATH_BUFFER];
	t_node		*parent;
	bool		active;
	size_t		i;

	/// GET PARENT
	if (g.node == NULL || g.node->parent == NULL)
		return;
	parent = g.node->parent;
	if (g.node->is_index == true) {
		if (parent->parent == NULL)
			return;
		parent = parent->parent;
	}
	/// COMPUTE SIBLINGS
	for (i = 0; i < parent->length; ++i) {
		if (parent->childs[i]->type == NODE_OTHER)
			continue;
		if (parent->childs[i]->is_index == true
		|| parent->childs[i]->is_template == true)
			continue;
		active = parent->childs[i] == g.node;
		if (compute_node_link(parent->childs[i], active, string) == false)
			continue;
		print_tag(param, true, fd_out);
		write(fd_out, string, str_len(string));
		print_tag(param, false, fd_out);
	}
}
