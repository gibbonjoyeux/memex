/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "memex.h"

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

void
print_block_image(char *file, int len, int *i, int fd_out) {
	int				j, k;

	/// GET IMAGE TITLE
	j = *i + 2;
	while (j < len && file[j] != ']')
		j += 1;
	/// ERROR
	if (j + 2 >= len
	|| file[j + 1] != '(') {
		print_block_paragraph(file, len, i, fd_out);
		return;
	}
	/// GET IMAGE URL
	k = j + 2;
	while (k < len && file[k] != ')')
		k += 1;
	/// ERROR
	if (k == len) {
		print_block_paragraph(file, len, i, fd_out);
		return;
	}
	/// PRINT IMAGE
	write(fd_out, "<img src=\"", 10);
	print_string(file + j + 2, k - j - 2, fd_out);
	write(fd_out, "\" alt=\"", 7);
	print_string(file + *i + 2, j - *i - 2, fd_out);
	/// WITH EXTRA
	if (g.extra[0] != 0) {
		write(fd_out, "\" ", 2);
		write(fd_out, g.extra, str_len(g.extra));
		g.extra[0] = 0;
		write(fd_out, ">", 1);
	/// NO EXTRA
	} else {
		write(fd_out, "\">", 2);
	}
	*i = k + 1;
}
