/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "memex.h"

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

void			compute_variable_tag(char *param, int fd_out) {
	char		string[VAR_BUFFER];
	int			len;

	/// ADD OPENING
	string[0] = '<';
	/// ADD TAG
	str_cpy(string + 1, param);
	len = str_len(string);
	/// ADD CLOSING
	string[len] = '>';
	/// WRITE LINK
	write(fd_out, string, len + 1);
}
