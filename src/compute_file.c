/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "memex.h"

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

static inline void
compute_path(char *path, t_node *node) {
	int				i, j;

	/// COPY BASE DIRECTORY
	str_cpy(path, "./build/");
	/// COPY MAIN PATH
	i = 8;
	j = 0;
	while (node->path[j] != 0) {
		path[i] = node->path[j];
		i += 1;
		j += 1;
	}
	path[i] = 0;
	if (str_equ(path + i - 4, ".txt") == true)
		str_cpy(path + i - 4, ".html");
}

static inline int
create_file(t_node *node) {
	char			path[PATH_BUFFER];
	int				fd;

	/// COMPUTE PATH
	compute_path(path, node);
	/// CREATE FILE
	fd = open(path, O_WRONLY | O_CREAT | O_TRUNC, 0666);
	return fd;
}

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

bool
compute_file(t_node *node) {
	bool			value;
	int				fd_out;

	/// RESET STACK
	g.tag.index = 0;
	/// EXIT IF TEMPLATE
	if (node->is_template == true)
		return true;
	/// CREATE OUTPUT FILE
	fd_out = create_file(node);
	if (fd_out < 0)
		return false;
	/// COMPUTE FILE
	if (node->type == NODE_TXT)
		value = compute_file_txt(node, fd_out);
	else if (node->type == NODE_HTML)
		value = compute_file_html(node->path, fd_out);
	else
		value = compute_file_other(node->path, fd_out);
	close(fd_out);
	return value;
}
