/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "memex.h"

static t_node			*build_directory(int);

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

static inline char	**list_directory(char *path, size_t *length, int index) {
	char			**files;
	t_dir_entry		*entry;
	t_dir			*dir;
	ssize_t			i;

	*length = 0;
	/// OPEN DIRECTORY
	dir = opendir(path);
	if (dir == NULL)
		return NULL;
	/// COMPUTE LENGTH
	*length = 0;
	while (true) {
		entry = readdir(dir);
		if (entry == NULL)
			break;
		if (index > 0 || str_equ(entry->d_name, "build") == false)
			*length += 1;
	}
	if (*length < 2)
		return NULL;
	*length -= 2;
	closedir(dir);
	/// CREATE CHILDS LIST
	files = calloc(sizeof(char*), *length);
	if (files == NULL)
		return NULL;
	/// RE-OPEN DIRECTORY
	dir = opendir(path);
	if (dir == NULL) {
		free(files);
		return NULL;
	}
	/// SKIP . & ..
	if (readdir(dir) == NULL || readdir(dir) == NULL) {
		free(files);
		return NULL;
	}
	/// FILL CHILDS LIST
	i = 0;
	while (true) {
		entry = readdir(dir);
		if (entry == NULL)
			break;
		if (index > 0 || str_equ(entry->d_name, "build") == false) {
			files[i] = str_dup(entry->d_name);
			if (files[i] == NULL) {
				for (i = i - 1; i >= 0; --i)
					free(files[i]);
				free(files);
				closedir(dir);
				return NULL;
			}
			i += 1;
		}
	}
	closedir(dir);
	return files;
}

static inline t_node	*build_file(u8 type) {
	t_node				*node;

	/// INIT NODE
	node = malloc(sizeof(t_node));
	if (node == NULL)
		return NULL;
	node->path = str_dup(g.path);
	node->type = type;
	node->parent = NULL;
	node->childs = NULL;
	node->index = NULL;
	node->length = 0;
	node->is_index = false;
	node->is_template = false;
	return node;
}

static inline t_node	**build_directory_childs(char **files,
						size_t length, int index) {
	t_node				**childs;
	t_stat				file_stat;
	size_t				len;
	size_t				i;

	/// ALLOC CHILDS
	childs = calloc(sizeof(t_node*), length);
	if (childs == NULL)
		return NULL;
	/// FILL CHILDS
	for (i = 0; i < length; ++i) {
		/// HANDLE DIRECTORY ENTRY
		str_cpy(g.path + index, files[i]);
		stat(g.path, &file_stat);
		/// ENTRY IS DIRECTORY
		if (S_ISDIR(file_stat.st_mode)) {
			len = str_len(files[i]);
			g.path[index + len] = '/';
			g.path[index + len + 1] = 0;
			childs[i] = build_directory(index + len + 1);
		/// ENTRY IS TXT
		} else if (is_txt(files[i]) == true) {
			childs[i] = build_file(NODE_TXT);
			childs[i]->is_index = (str_equ(files[i], "index.txt"));
		/// ENTRY IS HTML
		} else if (is_html(files[i]) == true) {
			childs[i] = build_file(NODE_HTML);
			childs[i]->is_index = (str_equ(files[i], "index.html"));
			childs[i]->is_template = (str_equ(files[i], "template.html"));
		/// ENTRY IS REGULAR FILE
		} else {
			childs[i] = build_file(NODE_OTHER);
		}
	}
	return childs;
}

static t_node			*build_directory(int index) {
	t_node				*node;
	char				**files;
	size_t				i;

	/// INIT NODE
	node = malloc(sizeof(t_node));
	if (node == NULL)
		return NULL;
	node->path = str_dup(g.path);
	if (node->path == NULL) {
		free(node);
		return NULL;
	}
	node->type = NODE_DIR;
	node->parent = NULL;
	node->childs = NULL;
	node->length = 0;
	/// PRE COMPUTE CHILDS
	files = list_directory(g.path, &(node->length), index);
	if (files == NULL) {
		free(node->path);
		free(node);
		return NULL;
	}
	/// COMPUTE CHILDS
	if (node->length > 0) {
		node->childs = build_directory_childs(files, node->length, index);
		if (node->childs == NULL) {
			for (i = 0; i < node->length; ++i)
				free(files[i]);
			free(files);
			free(node->path);
			free(node);
			return NULL;
		}
		/// SET CHILDS PARENT
		for (i = 0; i < node->length; ++i) {
			node->childs[i]->parent = node;
			if (node->childs[i]->is_index == true)
				node->index = node->childs[i];
		}
	}
	for (i = 0; i < node->length; ++i)
		free(files[i]);
	free(files);
	index = 0;
	return node;
}

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

t_node				*build_tree(void) {
	return build_directory(0);
}
