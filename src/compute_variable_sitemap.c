/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "memex.h"

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

static void		compute_generation(t_node *node, char *tag_1, char *tag_2,
				int fd_out) {
	char		string[PATH_BUFFER];
	size_t		i;

	/// COMPUTE CURRENT GENERATION
	if (tag_2[0] != 0)
		print_tag(tag_2, true, fd_out);
	/// COMPUTE SIBLINGS
	for (i = 0; i < node->length; ++i) {
		if (node->childs[i]->type == NODE_OTHER)
			continue;
		if (node->childs[i]->is_index == true
		|| node->childs[i]->is_template == true)
			continue;
		if (compute_node_link(node->childs[i], false, string) == false)
			continue;
		print_tag(tag_1, true, fd_out);
		write(fd_out, string, str_len(string));
		if (node->childs[i]->type == NODE_DIR)
			compute_generation(node->childs[i], tag_1, tag_2, fd_out);
		print_tag(tag_1, false, fd_out);
	}
	if (tag_2[0] != 0)
		print_tag(tag_2, false, fd_out);
}

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

void			compute_variable_sitemap(int fd_out) {
	/// COMPUTE TREE GENERATIONS
	compute_generation(g.tree, "li", "ul", fd_out);
}
