/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "memex.h"

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

static inline void
compute_tag_extra(char *file, int len, int *i, int fd_out) {
	int				j, k;

	/// SKIP SPACE
	j = *i + 2;
	while (j < len && (file[j] == ' ' || file[j] == '\t'))
		j += 1;
	/// ERROR
	if (j >= len) {
		print_block_paragraph(file, len, i, fd_out);
		return;
	}
	/// GET EXTRA CONTENT
	k = 0;
	while (true) {
		/// ERROR
		if (j >= len || k >= EXTRA_BUFFER) {
			g.extra[0] = 0;
			print_block_paragraph(file, len, i, fd_out);
			return;
		}
		/// END
		if (file[j] == '\n')
			break;
		/// COPY
		g.extra[k] = file[j];
		j += 1;
		k += 1;
	}
	/// OFFSET FILE
	g.extra[k] = 0;
	k -= 1;
	while (k > 0 && IS_SPACE(g.extra[k]) == true) {
		g.extra[k] = 0;
		k -= 1;
	}
	*i = j + 1;
}

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

bool
compute_file_html(char *path, int fd_out) {
	char			*file;
	int				fd_in;
	int				len;
	int				i;

	/// RESET EXTRA TAG
	g.extra[0] = 0;
	/// OPEN FILE IN
	fd_in = open(path, O_RDONLY);
	if (fd_in < 0)
		return false;
	len = read_file(fd_in, &file);
	if (len < 0) {
		close(fd_in);
		return false;
	}
	/// READ FILE
	i = 0;
	while (i < len) {
		while (i < len && IS_SPACE(file[i]) == true)
			i += 1;
		if (i == len)
			break;
		/// MD TITLE
		if (IS_TITLE(file, len, i) == true) {
			print_block_title(file, len, &i, fd_out);
		/// MD LINE
		} else if (IS_LINE(file, len, i) == true) {
			print_block_line(file, len, &i, fd_out);
		/// MD LIST
		} else if (IS_LIST(file, len, i) == true) {
			print_block_list(file, len, &i, fd_out);
		/// MD CHECKLIST
		} else if (IS_CHECKLIST(file, len, i) == true) {
			print_block_checklist(file, len, &i, fd_out);
		/// MD TABLE
		} else if (IS_TABLE(file, len, i) == true) {
			print_block_table(file, len, &i, fd_out);
		/// MD EXTRA
		} else if (IS_EXTRA(file, len, i) == true) {
			compute_tag_extra(file, len, &i, fd_out);
		/// MD TAG
		} else if (IS_TAG(file, len, i) == true) {
			print_block_tag(file, len, &i, fd_out);
		/// HTML HTML TAG
		} else if (IS_HTML(file, len, i) == true) {
			print_block_html(file, len, &i, fd_out);
		/// HTML IMAGE
		} else if (IS_IMAGE(file, len, i) == true) {
			print_block_image(file, len, &i, fd_out);
		/// MD PARAGRAPH
		} else {
			print_block_paragraph(file, len, &i, fd_out);
		}
	}
	return true;
}
