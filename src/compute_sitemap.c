/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "memex.h"

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

static inline int
create_file(char *name) {
	char			path[PATH_BUFFER];
	int				fd;

	/// COMPUTE PATH
	str_cpy(&(path[0]), "./build/");
	str_cpy(&(path[8]), name);
	/// CREATE FILE
	fd = open(path, O_WRONLY | O_CREAT | O_TRUNC, 0666);
	return fd;
}

static void
print_sitemap_page(char *buffer, t_node *node, int fd) {
	int			i;

	str_cpy(&(buffer[0]), g.url);
	str_cat(&(buffer[0]), "/");
	str_cat(&(buffer[0]), node->path);
	i = str_len(&(buffer[0]));
	/// NODE IS INDEX
	if (node->is_index == true) {
		while (i > 0 && buffer[i] != '/')
			i -= 1;
		i += 1;
	/// NODE IS TXT
	} else if (i >= 4 && str_equ(&(buffer[i]) - 4, ".txt") == true) {
		str_cpy(&(buffer[i]) - 4, ".html");
		i += 1;
	}
	write(fd, &(buffer[0]), i);
}

static void
print_sitemap_pages(char *buffer, t_node *node, int fd) {
	size_t		i;

	g.node = node;
	/// NODE DIRECTORY
	if (node->type == NODE_DIR) {
		for (i = 0; i < node->length; ++i)
			print_sitemap_pages(buffer, node->childs[i], fd);
	/// NODE FILE
	} else if (node->type == NODE_HTML || node->type == NODE_TXT) {
		if (node->is_template == false) {
			print_tag("url", true, fd);
				print_tag("loc", true, fd);
					print_sitemap_page(buffer, node, fd);
				print_tag("loc", false, fd);
			print_tag("url", false, fd);
		}
	}
}

static void
print_sitemap(char *buffer, t_node *node, int fd) {
	print_tag("?xml version=\"1.0\" encoding=\"UTF-8\"?", true, fd);
	print_tag("urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\"",
	/**/ true, fd);
	print_sitemap_pages(buffer, node, fd);
	print_tag("urlset", false, fd);
}

static void
print_robots(char *buffer, int fd) {
	str_cpy(buffer, "User-agent: *\nSitemap: ");
	str_cat(buffer, g.url);
	str_cat(buffer, "/sitemap.xml");
	write(fd, buffer, str_len(buffer));
}

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

bool			compute_sitemap(t_node *node) {
	char		buffer[PATH_BUFFER];
	int			fd;

	/// CREATE SITEMAP FILE
	fd = create_file("sitemap.xml");
	if (fd < 0)
		return false;
	print_sitemap(&(buffer[0]), node, fd);
	close(fd);
	/// CREATE ROBOTS FILE
	fd = create_file("robots.txt");
	if (fd < 0)
		return false;
	print_robots(&(buffer[0]), fd);
	close(fd);
	return true;
}
