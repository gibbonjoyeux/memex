/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "memex.h"

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

static void
print_block_list_level(char *file, int len, int *i, int fd_out, int level) {
	int				j, k;

	/// OPEN LIST
	j = *i;
	while (j < len && file[j] == '\t')
		j += 1;
	if (j == len || j - *i != level || file[j] != '-')
		return;
	print_tag("ul", true, fd_out);
	/// LOOP THROUGH LIST
	while (true) {
		/// SKIP SPACE BEFORE CONTENT
		j += 1;
		while (j < len && IS_SPACE(file[j]) == true)
			j += 1;
		/// GET ITEM CONTENT
		k = j;
		while (k < len) {
			if (file[k] == '<')
				skip_html(file, len, &k);
			if (file[k] == '\n')
				break;
			k += 1;
		}
		/// OFFSET FILE
		*i = k;
		if (k < len)
			*i += 1;
		/// PRINT ITEM CONTENT (RECURSIVE)
		write(fd_out, "<li>", 4);
		print_string(file + j, k - j, fd_out);
		print_block_list_level(file, len, i, fd_out, level + 1);
		write(fd_out, "</li>\n", 6);
		/// CHECK LEVEL
		j = *i;
		while (j < len && file[j] == '\t')
			j += 1;
		if (j == len || j - *i != level || file[j] != '-')
			break;
	}
	/// CLOSE LIST
	write(fd_out, "</ul>\n", 6);
}

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

void
print_block_list(char *file, int len, int *i, int fd_out) {
	print_block_list_level(file, len, i, fd_out, 0);
}
