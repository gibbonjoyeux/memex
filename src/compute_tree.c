/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "memex.h"

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

static inline bool	create_directory(t_node *node) {
	char			path[PATH_BUFFER];
	t_stat			dir_stat;

	/// COMPUTE PATH
	str_cpy(path, "./build/");
	if (node->parent != NULL)
		str_cat(path, node->path);
	/// CREATE DIRECTORY (IF DOES NOT EXIST)
	if (stat(path, &dir_stat) < 0)
		if (mkdir(path, 0777) < 0)
			return false;
	return true;
}

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

bool			compute_tree(t_node *node) {
	size_t		i;

	g.node = node;
	/// NODE DIRECTORY
	if (node->type == NODE_DIR) {
		/// CREATE DIRECTORY
		if (create_directory(node) == false)
			return false;
		for (i = 0; i < node->length; ++i)
			if (compute_tree(node->childs[i]) == false)
				return false;
		return true;
	/// NODE FILE
	} else {
		return compute_file(node);
	}
}
