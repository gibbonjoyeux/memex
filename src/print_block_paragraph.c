/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "memex.h"

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

void
print_block_paragraph(char *file, int len, int *i, int fd_out) {
	bool			empty;
	int				j;
	
	j = *i;
	while (true) {
		empty = true;
		while (j < len && file[j] != '\n') {
			if (IS_SPACE(file[j]) == false)
				empty = false;
			if (file[j] == '<')
				skip_html(file, len, &j);
			j += 1;
		}
		if (j == len || empty == true)
			break;
		j += 1;
	}
	print_tag("p", true, fd_out);
	print_string(file + *i, j - *i - 1, fd_out);
	write(fd_out, "</p>", 4);
	*i = j;
	if (j < len && file[j] == '\n')
		*i += 1;
}
