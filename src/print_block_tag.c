/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "memex.h"

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

void
print_block_tag(char *file, int len, int *i, int fd_out) {
	char			name[STACK_NAME_BUFFER];
	int				j, k;

	/// SKIP SPACE
	j = *i + 2;
	while (j < len && (file[j] == ' ' || file[j] == '\t'))
		j += 1;
	/// GET TAG NAME
	k = 0;
	while (j < len && k + 1 < STACK_NAME_BUFFER && IS_ALPHA(file[j])) {
		name[k] = file[j];
		j += 1;
		k += 1;
	}
	/// ERROR
	if (j >= len || k >= STACK_NAME_BUFFER) {
		print_block_paragraph(file, len, i, fd_out);
		return;
	}
	name[k] = 0;
	/// POP STACK
	if (g.tag.index > 0
	&& str_equ(name, g.tag.stack[g.tag.index - 1]) == true) {
		g.tag.index -= 1;
		print_tag(name, false, fd_out);
	/// PUSH STACK
	} else {
		if (g.tag.index >= STACK_BUFFER) {
			print_block_paragraph(file, len, i, fd_out);
			return;
		}
		str_cpy(g.tag.stack[g.tag.index], name);
		g.tag.index += 1;
		print_tag(name, true, fd_out);
	}
	*i = j + 1;
}
