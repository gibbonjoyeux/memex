/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "memex.h"

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

void
print_block_table(char *file, int len, int *i, int fd_out) {
	int				j, k;
	bool			header;

	/// COMPUTE TABLE
	print_tag("table", true, fd_out);
	while (true) {
		j = *i;
		if (j == len || file[j] != '|')
			break;
		j += 1;
		if (j < len && file[j] == '#') {
			header = true;
			j += 1;
		}
		/// COMPUTE ROW
		write(fd_out, "<tr>\n", 5);
		while (true) {
			/// GET ROW CONTENT
			k = j;
			while (k < len) {
				if (file[k] == '<')
					skip_html(file, len, &k);
				if (file[k] == '|' || file[k] == '\n')
					break;
				k += 1;
			}
			//if (k < len && file[k] == '\n')
			//	break;
			/// PRINT ROW
			if (header == true) {
				write(fd_out, "<th>", 4);
				print_string(file + j, k - j, fd_out);
				write(fd_out, "</th>", 5);
			} else {
				write(fd_out, "<td>", 4);
				print_string(file + j, k - j, fd_out);
				write(fd_out, "</td>", 5);
			}
			if (k == len || file[k] == '\n')
				break;
			j = k + 1;
		}
		write(fd_out, "</tr>\n", 6);
		*i = k;
		if (k < len)
			*i += 1;
		header = false;
	}
	write(fd_out, "</table>\n", 9);
}
