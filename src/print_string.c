/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "memex.h"

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

static inline void
print_string_variable(char *variable, int len, int fd_out) {
	char			name[VAR_BUFFER];
	char			param[VAR_BUFFER];
	int				i, j;

	/// GET VARIABLE NAME
	i = 0;
	while (i < len && variable[i] != ':' && variable[i] != '}') {
		name[i] = variable[i];
		i += 1;
	}
	name[i] = 0;
	/// GET VARIABLE PARAM
	if (i < len && variable[i] == ':') {
		i += 1;
		j = 0;
		while (i < len && variable[i] != '}') {
			param[j] = variable[i];
			i += 1;
			j += 1;
		}
		param[j] = 0;
	} else {
		param[0] = 0;
	}
	/// CHECK VARIABLE
	if (str_equ(name, "TITLE") == true)
		compute_variable_title(fd_out);
	else if (str_equ(name, "SIBLINGS") == true)
		compute_variable_siblings(param, fd_out);
	else if (str_equ(name, "CHILDS") == true)
		compute_variable_childs(fd_out);
	else if (str_equ(name, "TREE") == true)
		compute_variable_tree(param, fd_out);
	else if (str_equ(name, "SITEMAP") == true)
		compute_variable_sitemap(fd_out);
	else if (str_equ(name, "URL") == true)
		compute_variable_url(fd_out);
	else if (str_equ(name, "LINK") == true)
		compute_variable_link(param, fd_out);
	else if (str_equ(name, "TAG") == true)
		compute_variable_tag(param, fd_out);
	else if (str_equ(name, "CONTENT") == true)
		compute_variable_content(fd_out);
	else
		compute_variable_template(name, fd_out);
}

static inline void
print_string_variables(char *file, int len, int fd_out) {
	int				i, j;

	i = 0;
	j = 0;
	while (j < len) {
		/// CHECK VARIABLE
		if (file[j] == '$') {
			/// FOUND VARIABLE
			if (j < len - 2 && file[j + 1] == '{') {
				/// FLUSH
				write(fd_out, file + i, j - i);
				/// WRITE VARIABLE
				print_string_variable(file + j + 2, len - j - 2, fd_out);
				/// SKIP VARIABLE
				j += 2;
				while (j < len && file[j] != '}')
					j += 1;
				i = j + 1;
			}
		}
		j += 1;
	}
	/// FLUSH
	write(fd_out, file + i, j - i);
}

static inline void
print_emphasis(char *file, int *i, int *j, bool *emphasis, int fd_out) {
	/// FLUSH
	print_string_variables(file + *i, *j - *i, fd_out);
	/// PUT EMPHASIS
	if (*emphasis == false)
		write(fd_out, "<em>", 4);
	else
		write(fd_out, "</em>", 5);
	/// UPDATE
	*emphasis = !(*emphasis);
	*j += 1;
	*i = *j;
}

static inline void
print_link(char *file, int len, int *i, int *j, int fd_out) {
	int				k, l;

	/// GET LINK TEXT
	k = *j + 1;
	while (k < len && file[k] != ']')
		k += 1;
	/// ERROR
	if (k + 2 >= len
	|| file[k + 1] != '(')
		return;
	/// GET LINK URL
	l = k + 2;
	while (l < len && file[l] != ')')
		l += 1;
	/// ERROR
	if (l >= len)
		return;
	/// FLUSH
	print_string_variables(file + *i, *j - *i, fd_out);
	/// PRINT LINK
	dprintf(fd_out, "<a href=\"%.*s\">%.*s</a>",
	/**/ l - k - 2, file + k + 2,
	/**/ k - *j - 1, file + *j + 1);
	/// OFFSET FILE
	*j = l + 1;
	*i = *j;
}

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

void
print_string(char *file, int len, int fd_out) {
	bool			emphasis;
	int				i, j;

	emphasis = false;
	i = 0;
	j = 0;
	while (j < len) {
		/// EMPHASIS
		if (file[j] == '*')
			print_emphasis(file, &i, &j, &emphasis, fd_out);
		/// LINK
		else if (file[j] == '[')
			print_link(file, len, &i, &j, fd_out);
		/// HTML BLOCK
		else if (file[j] == '<')
			skip_html(file, len, &j);
		j += 1;
	}
	/// FLUSH
	print_string_variables(file + i, len - i, fd_out);
	/// FLUSH EMPHASIS
	if (emphasis == true)
		write(fd_out, "</em>", 5);
}
