/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "memex.h"

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

static void		compute_generation(t_node *parent, t_node *child,
				char *tag_1, char *tag_2, int fd_out) {
	char		string[PATH_BUFFER];
	bool		active;
	size_t		i;

	/// COMPUTE PREVIOUS GENERATION
	if (parent->parent != NULL)
		compute_generation(parent->parent, parent, tag_1, tag_2, fd_out);
	/// COMPUTE CURRENT GENERATION
	if (tag_2[0] != 0)
		print_tag(tag_2, true, fd_out);
	/// COMPUTE SIBLINGS
	for (i = 0; i < parent->length; ++i) {
		if (parent->childs[i]->type == NODE_OTHER)
			continue;
		if (parent->childs[i]->is_index == true
		|| parent->childs[i]->is_template == true)
			continue;
		active = (parent->childs[i] == child);
		if (compute_node_link(parent->childs[i], active, string) == false)
			continue;
		print_tag(tag_1, true, fd_out);
		write(fd_out, string, str_len(string));
		print_tag(tag_1, false, fd_out);
	}
	if (tag_2[0] != 0)
		print_tag(tag_2, false, fd_out);
}

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

void			compute_variable_tree(char *param, int fd_out) {
	char		tag_1[VAR_BUFFER];
	char		tag_2[VAR_BUFFER];
	t_node		*node;
	int			i, j;

	/// GET PARENT
	if (g.node == NULL)
		return;
	node = g.node;
	/// COMPUTE PARAMETERS
	tag_1[0] = 0;
	tag_2[0] = 0;
	i = 0;
	while (param[i] != 0 && param[i] != ':') {
		tag_1[i] = param[i];
		i += 1;
	}
	tag_1[i] = 0;
	if (param[i] == ':') {
		i += 1;
		j = 0;
		while (param[i + j] != 0) {
			tag_2[j] = param[i + j];
			j += 1;
		}
		tag_2[j] = 0;
	}
	/// COMPUTE ALL GENERATIONS
	compute_generation(node, node, tag_1, tag_2, fd_out);
}
