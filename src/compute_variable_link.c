/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "memex.h"

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

void			compute_variable_link(char *param, int fd_out) {
	char		string[VAR_BUFFER];
	int			i, j, k;
	int			len;

	/// ADD OPENING
	str_cpy(string, "<a href=\"");
	/// ADD URL
	j = 9;
	i = 0;
	while (param[i] != 0) {
		if (param[i] == ',')
			break;
		string[j + i] = param[i];
		i += 1;
	}
	str_cpy(string + i + j, "\">");
	j += i + 2;
	/// ADD NAME (SPECIFIED)
	if (param[i] == ',') {
		i += 1;
		k = 0;
		while (param[i + k] != 0) {
			string[j + k] = param[i + k];
			k += 1;
		}
		len = j + k;
	/// ADD NAME (URL)
	} else {
		i = 0;
		while (param[i] != 0) {
			string[j + i] = param[i];
			i += 1;
		}
		len = i + j;
	}
	/// ADD CLOSING
	str_cpy(string + len, "</a>");
	/// WRITE LINK
	write(fd_out, string, len + 4);
}
