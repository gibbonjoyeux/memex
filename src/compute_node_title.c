/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "memex.h"

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

void			compute_node_title(t_node *node, char *buffer) {
	char		*path;
	ssize_t		len;
	size_t		i, j, k;

	path = node->path;
	if (node->is_index == true)
		path = node->parent->path;
	/// GET FILE NAME
	len = str_len(path);
	i = len - 1;
	j = len;
	if (path[i] == '/') {
		i -= 1;
		j -= 1;
	}
	while (i > 0 && path[i] != '/') {
		if (path[i] == '.')
			j = i;
		i -= 1;
	}
	if (path[i] == '/')
		i += 1;
	/// PUT TITLE
	str_ncpy(buffer, path + i, j - i);
	buffer[j - i] = 0;
	/// REPLACE DASHES
	for (k = 0; k < j - i; ++k)
		if (buffer[k] == '-' || buffer[k] == '_')
			buffer[k] = ' ';
}
