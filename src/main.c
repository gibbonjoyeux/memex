/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "memex.h"

t_memex				g;

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

static void			draw_tree(t_node *node, int level) {
	size_t			i;

	printf("%*s%s ", level * 4, "", node->path);
	if (node->type == NODE_DIR) {
		printf("(DIR)\n");
		for (i = 0; i < node->length; ++i)
			draw_tree(node->childs[i], level + 1);
	} else if (node->type == NODE_TXT) {
		printf("(TXT)\n");
	} else if (node->type == NODE_HTML) {
		if (node->is_template == true)
			printf("(HTML) TEMPLATE\n");
		else
			printf("(HTML)\n");
	} else {
		printf("(OTHER)\n");
	}
}

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

int					main(int argc, char **argv) {
	/// HANDLE PARAMETERS
	str_cpy(g.url, "/");
	if (argc > 1) {
		/// PARAM 1: Site directory
		if (chdir(argv[1]) < 0)
			return 1;
		/// PARAM 2: Url
		if (argc >= 2) {
			str_cpy(g.url, argv[2]);
			//str_cat(g.url, "/");
		}
	}
	/// INIT PATH
	str_cpy(g.path, "./");
	g.index = 2;
	chdir(g.path);
	/// BUILD TREE
	g.tree = build_tree();
	if (g.tree == NULL) {
		return 1;
	}
	/// DRAW TREE
	draw_tree(g.tree, 0);
	/// COMPUTE TREE
	if (compute_tree(g.tree) == false) {
		printf("ERROR\n");
		return 1;
	}
	/// COMPUTE SITEMAP FILES
	if (compute_sitemap(g.tree) == false) {
		printf("ERROR\n");
		return 1;
	}
	printf("DONE\n");
	return 0;
}
