/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "memex.h"

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

static inline bool
is_self_closing(char *file, char *tag, int i) {
	if (file[i - 1] == '/')
		return true;
	if (str_equ(tag, "area") == true
	|| str_equ(tag, "base") == true
	|| str_equ(tag, "col") == true
	|| str_equ(tag, "embed") == true
	|| str_equ(tag, "hr") == true
	|| str_equ(tag, "img") == true
	|| str_equ(tag, "input") == true
	|| str_equ(tag, "link") == true
	|| str_equ(tag, "meta") == true
	|| str_equ(tag, "param") == true
	|| str_equ(tag, "source") == true
	|| str_equ(tag, "track") == true
	|| str_equ(tag, "wbr") == true
	|| str_equ(tag, "!") == true			// Comment
	|| str_equ(tag, "!DOCTYPE") == true)	// Doctype HTML
		return true;
	return false;
}

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

void
skip_html(char *file, int len, int *i) {
	char			tag[32];
	int				j, k;

	/// GET TAG NAME
	j = *i + 1;
	while (j < len && j - *i < 32
	&& (IS_ALNUM(file[j]) == true || file[j] == '!')) {
		tag[j - *i - 1] = file[j];
		j += 1;
	}
	tag[j - *i - 1] = 0;
	/// ERROR
	if (j >= len || j - *i >= 31) {
		return;
	}
	/// SKIP TAG OPTIONS (CLASS, ID, ....)
	while (j < len && file[j] != '>')
		j += 1;
	/// ERROR
	if (j >= len)
		*i += 1;
	/// SELF CLOSING TAG
	if (is_self_closing(file, tag, j) == true) {
		*i = j + 1;
		return;
	}
	/// REGULAR TAG
	j += 1;
	while (j < len) {
		if (file[j] == '<') {
			/// TAG CLOSE
			if (j + 1 < len && file[j + 1] == '/') {
				j += 2;
				k = 0;
				while (j + k < len && tag[k] != 0 && tag[k] == file[j + k])
					k += 1;
				/// ERROR
				if (j + k >= len) {
					*i = len;
					return;
				}
				/// TAG CLOSE FOUND
				if (tag[k] == 0 && file[j + k] == '>') {
					*i = j + k + 1;
					return;
				}
				/// TAG CLOSE NOT FOUND
				j += 1;
			/// TAG NEW (RECURSIVE)
			} else {
				k = j;
				skip_html(file, len, &j);
				if (j == k)
					j += 1;
			}
		} else {
			j += 1;
		}
	}
}
