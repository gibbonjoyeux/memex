#!/usr/bin/env bash
############################### BY GIBBONJOYEUX ################################

################################################################################
### FUNCTIONS
################################################################################

################################################################################
### MAIN
################################################################################

echo CLOSE SUBMODULES
git submodule init
git submodule update --remote --merge

echo BUILD LIBRARIES
cd lib/c-basics/
./build.sh
cd ../c-string/
./build.sh
cd ../../

echo BUILD MEMEX
gcc -o memex \
	src/*.c \
	-I inc/ \
	-I lib/c-basics/inc/ \
	-I lib/c-string/inc/ \
	lib/c-basics/c-basics.a \
	lib/c-string/c-string.a
